﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public float speed = 10f;
	public Vector2 maxVelocity = new Vector2(3, 5); 
	private PlayerController controller;
	private Animator animator;
	// Use this for initialization
	void Start () {
		controller = GetComponent<PlayerController>();
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		var forceX = 0f;
		var forceY = 0f;

		var absVelocityX = Mathf.Abs(rigidbody2D.velocity.x);
		var absVelocityY = Mathf.Abs(rigidbody2D.velocity.y);


		if (controller.moving.x != 0) {
			if (absVelocityX < maxVelocity.x) {
				forceX = speed * controller.moving.x;
				transform.localScale = new Vector3 (forceX > 0 ? 1 : -1, 1, 1);
			}
			//TODO: Set animation state for left right here
		} else if (controller.moving.y != 0) {
			if (absVelocityY < maxVelocity.y) {
				forceY = speed * controller.moving.y;
			}
			bool movingUp = forceY > 0;
			//TODO: Set the animation state to up or down
		} else {
			//TODO: Set animation state to idle standing state
		}

		rigidbody2D.velocity = new Vector2(forceX, forceY);
	}
}
