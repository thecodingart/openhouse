﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour {
	private ArrayList items = ArrayList();
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool AddItem(GameObject item) {

		bool addedItem = false;
		if (item && items.Count < 1) {
			items.Add(item);
			addedItem = true;
		}

		return addedItem;
	}
}
