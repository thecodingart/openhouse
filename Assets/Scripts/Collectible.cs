﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D target) {
		if (target.gameObject.tag == "Player") {
			Inventory inventory = target.gameObject.GetComponent<Inventory>();
			if (inventory.AddItem(this)) {
				this.transform.parent = target.gameObject.transform;
			}
		}
	}
}
