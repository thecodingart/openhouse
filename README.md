# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a top-down collection game, racing against the clock, developed on Unity. ---------------- Driving through the suburbs in a stolen big white van, you see a tendril of smoke rise from a nearby house. You drive up to discover the door is wide open. Getting out and going into the house, you find it full of amazing STUFF that you would like, and the fire is pretty small right now. The security system announces, “Fire department contacted. Estimated time of arrival: two minutes.” You decide to take everything you can before the authorities arrive, since the items would burn down anyways or be ruined by water. You go through the house, collect objects one at a time and take them to your van. Each item adds an individual dollar value to your total upon going into van. The fire spreads in the meantime, blocking you off from objects. If you get too close, then you catch on fire! You can only take so many hits of fire before you die. Your gravestone will read "Unknown Looter." At 2:00 into the game, if you are back at the van, you peel out with a cache of stolen items as the fire department and police arrive. If you’re not back at the van at game end, a policeman arrests you.

See more: [Global Game Jam Site](http://globalgamejam.org/2015/games/open-house)

### How do I get set up? ###

This project was built with Unity 2.5

